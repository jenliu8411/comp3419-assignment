import processing.video.*;
import beads.*;
import ddf.minim.*;

Movie m;
int framenumber = 1;
int bgctr = 1;
int phase = 1;
float spring = 0.4;
float friction = -0.95;
Point centre = new Point(-1, -1);
Point head = new Point(0,0);
Point left_hand = new Point(0,0);
Point right_hand = new Point(0,0);
Point left_foot = new Point(0,0);
Point right_foot = new Point(0,0);
int numChick = 0, newSize = 10;
Chicken [] newChickens = new Chicken[newSize];
PImage monkeyFrame;
PImage bg;
PImage monkey_head_right,monkey_head_left, monkey_body, shoe_left, shoe_right, hand_left, hand_right;
PImage chicken ;
//PImage chicken;
int left, right, top, bottom;
int monkey_x = 30, monkey_y = 30;
boolean face_dir ;

AudioContext ac;
// this will hold the path to our audio file
String sourceFile;
// the SamplePlayer class will play the audio file
SamplePlayer sp;
Gain g;
Glide gainValue;
Minim minim;
AudioInput in;
AudioRecorder recorder;

void setup(){
  size(640,480);
  frameRate(120);
  m = new Movie(this, sketchPath("Usavich-short.mov"));
  //image(m, 1270,720);
  m.frameRate(120);
  framenumber = 0;
  m.play();
  monkey_head_right = loadImage("usavich-right.png");
  monkey_head_left = loadImage("usavich-left.png");
  monkey_body= loadImage("usavich-body.png");
  shoe_left = loadImage("foot-left.png");
  shoe_right = loadImage("foot-right.png");
  hand_left = loadImage("hand-left.png");
  hand_right = loadImage("hand-right.png");
  chicken = loadImage("chicken.png");
  ac = new AudioContext();
  sourceFile = sketchPath("") + "boing.wav";
   try {
    sp = new SamplePlayer(ac, new Sample(sourceFile));
   }
   catch(Exception e)
   {
     // If there is an error, show an error message
     // at the bottom of the processing window.
     println("Exception while attempting to load sample!");
     e.printStackTrace(); // print description of the error
     exit(); // and exit the program
   }
   
   sp.setKillOnEnd(false);
   gainValue = new Glide(ac, 0.0, 10);
   g = new Gain(ac, 1, gainValue);
   g.addInput(sp); 
   ac.out.addInput(g); 

   minim = new Minim(this);
   in = minim.getLineIn(Minim.STEREO, 2048);
   recorder = minim.createRecorder(in, "monkey.wav", true);

}

/*
1. Get the monkey's body
2. Change it to own picture
3. Create object to interact with it
4. When interaction happens, make a sound
*/
void draw() {
  float time = m.time();
  float duration = m.duration();
  
  if(time >= duration){
      if(phase == 1){
          m = new Movie(this, sketchPath("monkey.mov"));
          m.frameRate(60); 
          m.play();
          phase = 2;
          recorder.beginRecord();
          bgctr = framenumber;
          framenumber =1;
      }else if(phase == 2){
        recorder.endRecord();
        recorder.save();
        exit();
      }
  }
  
  if(m.available()){
      //background(0,0,0);
      m.read();
      
      if(phase ==1){
        
        image(m,0,0);
        m.save(sketchPath("") + "BG/"+nf(framenumber, 4) + ".tif");        
      }else if(phase == 2){
        
        monkeyFrame = removeBackground(m);
        bg = loadImage(sketchPath("") + "BG/"+nf(framenumber % bgctr, 4) + ".tif");
        // Overwrite the background 
          for (int x = 0; x < monkeyFrame.width; x++)
            for (int y = 0; y < monkeyFrame.height; y++){
              int mloc = x + y * monkeyFrame.width;
                  color mc = monkeyFrame.pixels[mloc];

                  if (mc != -1) {
                        // To control where you draw the monkey
                        // You can tweak the destination position of the monkey like
                        int bgx = constrain(x + 200, 0, bg.width);
                        int bgy = constrain(y + 60, 0, bg.height);
                      int bgloc = bgx + bgy * bg.width;
                      bg.pixels[bgloc] = mc;
                  }
            }
        bg.updatePixels();
        image(bg, 0, 0);
      }
      framenumber++; 
      
      if(phase == 2){
         
        //rabbit's cage
        stroke(155);
        strokeWeight(2);
        for(int i = 0; i < 5; i++ ){
          line((right-left)*i/4+monkey_x+left,top+monkey_y,(right-left)*i/4+monkey_x+left,bottom+monkey_y);
        }
        line(left+monkey_x,top+monkey_y,right+monkey_x,top+monkey_y);
        line(right+monkey_x,bottom+monkey_y,left+monkey_x,bottom+monkey_y);
        
        //draw rabbit's hands and feet
        image(shoe_left,left_foot.x+monkey_x-20,left_foot.y+monkey_y-20,30,30 );
        image(shoe_right,right_foot.x+monkey_x-10,right_foot.y+monkey_y-10,30,30 );
        image(hand_left,left_hand.x+monkey_x-20,left_hand.y+monkey_y-20,30,30);
        image(hand_right,right_hand.x+monkey_x-20,right_hand.y+monkey_y-20,30,30);
        
        //draw rabbit's body 
        stroke(228,181,151);
        strokeWeight(7);
        line(left_hand.x+monkey_x,left_hand.y+monkey_y,centre.x+monkey_x,centre.y+monkey_y);
        line(right_hand.x+monkey_x,right_hand.y+monkey_y,centre.x+monkey_x,centre.y+monkey_y);
        line(left_foot.x+monkey_x,left_foot.y+monkey_y,centre.x+monkey_x,centre.y+monkey_y);
        line(right_foot.x+monkey_x,right_foot.y+monkey_y,centre.x+monkey_x,centre.y+monkey_y);
        image(monkey_body,centre.x+monkey_x-20,centre.y+monkey_y-20,40,50 );
        
        //draw rabbit's face
        if (face_dir == true){
            image(monkey_head_right,centre.x+monkey_x-30,centre.y+monkey_y-90,50,80);
        }else{
            image(monkey_head_left,centre.x+monkey_x-30,centre.y+monkey_y-90,50,80);
        }
        
        //draw chicken
        for(int i =0; i < numChick; i++) {
          newChickens[i].collide();
          newChickens[i].move();
          newChickens[i].display();
        }
      }
      saveFrame(sketchPath("") + "/composite/" + nf(framenumber, 4) + ".tif");

    }
}
void mouseClicked(){
  //when the mouse is clicked, create a new chicken
  if(numChick < newSize){
      float vx = random(-30,30);
      float vy = random(-30,30);
      
      float din = 100;  

      newChickens[numChick] = new Chicken(mouseX, mouseY, din, numChick, newChickens,vx,vy);  
      numChick ++;
    } else {
     System.out.println("no more ball"); 
    }
}
void movieEvent(Movie m) { 
} 

PImage removeBackground(PImage frame) {
  int count = 0, totalx = 0, totaly = 0; 
  left = frame.width;
  right = 0;
  top = frame.height;
  bottom = 0;
  int orig_headx = centre.x;
  
  for (int x = 0; x < frame.width; x ++) for (int y = 0; y < frame.height; y ++) {
      int loc = x + y * frame.width;
      color c = frame.pixels[loc];
      //find the blue background and set to invisible
      if ( blue(c) * 2 > red(c) + green(c) + 40){ 
              frame.pixels[loc] = -1; 
      }else{
          //set boundary of the monkey
          if(x < left) left =x;
          if(x > right) right = x;
          if(y > bottom) bottom = y;
          if(y < top) top = y;
          //get the sum of the monkey's coordinates when red pixels are found
          if (red(c) > 192 && green(c) < 144) {
          totalx += x;
          totaly += y; 
          count ++;
          }
      }      
    }
  //calculate monkey's centre
  centre = new Point(totalx/count, totaly/count);

  //if monkey moves to the left, turn the face to the left
  if(orig_headx < centre.x - 5 ){
    face_dir = true;
  //else turn the face to the right
  }else if(orig_headx > centre.x + 5){
    face_dir = false;
  }

  monkey_position(left_hand,left,centre.x,top,centre.y);
  monkey_position(right_hand,centre.x,right,top,centre.y);
  monkey_position(left_foot,left,centre.x,centre.y,bottom);
  monkey_position (right_foot,centre.x, right, centre.y, bottom);
  
  frame.updatePixels();
  return frame;
}

//get the hands and foots of the monkey
Point monkey_position(Point body, int x1, int x2, int y1, int y2 ){
  int counter = 1;
  for(int x = x1; x < x2; x++){
    for(int y = y1; y < y2; y++){
      int loc = x + y*m.width;
      color c = m.pixels[loc];
      if (red(c) > 192 && green(c) < 144) {
             body.x +=x;
             body.y +=y;
             counter ++;
      }
    }
  }
   body.x /=  counter;
   body.y /=  counter;
  return body;
  
}
class Chicken{
  float x, y;
  float diameter;
  float vx = 0;
  float vy = 0;
  int id;
  Chicken[] others;
 
  Chicken(float xin, float yin, float din, int idin, Chicken[] oin, float vxin, float vyin) {
    x = xin;
    y = yin;
    diameter = din;
    id = idin;
    others = oin;
    vx = vxin;
    vy = vyin;
  } 
  
  void collide(){
      //chicken and chicken's collide
      for (int i = id + 1; i < numChick; i++) {
      float dx = others[i].x - x;
      float dy = others[i].y - y;
      float distance = sqrt(dx*dx + dy*dy);
      float minDist = others[i].diameter/2 + diameter/2;
      //if the distance between chcikens is smaller than one chicken's radius 
      //then collision happens
      if (distance < minDist) { 
        float angle = atan2(dy, dx);
        float targetX = x + cos(angle) * minDist;
        float targetY = y + sin(angle) * minDist;
        float ax = (targetX - others[i].x) * spring;
        float ay = (targetY - others[i].y) * spring;
        vx -= ax;
        vy -= ay;
        others[i].vx += ax;
        others[i].vy += ay;
      }
    }   
    //chicken and cage's collision
    float dy = y - (centre.y + monkey_y);
    float dx = x - (centre.x + monkey_x);
    //set the boundary of the cage by four diagonse lines
    if( x - diameter/2 < (right + monkey_x) && x + diameter/2 > (left + monkey_x) && y - diameter/2 < (bottom + monkey_y) && y + diameter/2> (top + monkey_y) ){
        //if the chicken hits the cage, make the sound
        try {
          sp = new SamplePlayer(ac, new Sample(sourceFile));
        }
        catch(Exception e)
        {
         println("Exception while attempting to load sample!");
         e.printStackTrace(); // print description of the error
         exit(); // and exit t
        }  
       
        g.addInput(sp); // connect the SamplePlayer to the Gain
        ac.out.addInput(g); // connect the Gain to the AudioContext
        ac.start(); // begin audio processing
        gainValue.setValue(0.2);
        sp.setToLoopStart();
        sp.start();
        
      //set the bounce back direction of the chicken when hitting the cage
      
      //top and bottom, opposite y direction
      if( (dy > dx && dy > -dx && vy < 0) || (dy < dx && dy < -dx && vy > 0)){
         vy = -vy;
         y += vy;
      //left and right, opposite x direction 
      }else if( (dy < -dx && dy > dx && vx > 0) || (dy > -dx && dy < dx && vx <0)){
         vx = -vx; 
         x += vx;
      }

      
    }
      
  }
  void move(){
    x += vx;
    y += vy;
    if (x + diameter/2 > width) {
      x = width - diameter/2;
      vx *= friction; 
    }
    else if (x - diameter/2 < 0) {
      x = diameter/2;
      vx *= friction;
    }
    if (y + diameter/2 > height) {
      y = height - diameter/2;
      vy *= friction; 
    } 
    else if (y - diameter/2 < 0) {
      y = diameter/2;
      vy *= friction;
    }
  }
  void display(){
     image(chicken,x - diameter/2,y - diameter/2,diameter,diameter);
  }
}

class Point{
  public int x, y;
  Point(int x, int y){
    this.x = x;
    this.y = y;
  }
}