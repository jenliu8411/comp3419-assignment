# README #

### What is this repository for? ###

This repository is an assignment for the multimedia subject. The aim of the assignment is to track the motion of a monkey in a given video, create a marionette to follow the motion of it, and have interacting objects with the marionette.

### How do I get set up? ###

1. Download Processing
2. Open the .pde file in the folder
3. Press the Play button
4. When the monkey appears on the screen, click anywhere on the screen to shoot a chicken
   ie. when a chicken touches the cage of the rabbit, it will bounce back in opposite direction and random velocity